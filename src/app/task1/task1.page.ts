import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TasksService } from '../tasks.service';


@Component({
  selector: 'app-task1',
  templateUrl: './task1.page.html',
  styleUrls: ['./task1.page.scss'],
})
export class Task1Page implements OnInit {
  title: string;
  text: string;

  constructor(public activatedRoute: ActivatedRoute, public tasksService: TasksService) { 

  }

  ngOnInit() {
    const id: number = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
    let display: any = this.tasksService.getElement(id);

    this.title = display.title;
    this.text = display.text;
  }

}
